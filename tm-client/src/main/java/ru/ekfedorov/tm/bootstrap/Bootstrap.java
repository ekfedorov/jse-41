package ru.ekfedorov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.api.repository.*;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.command.system.ExitCommand;
import ru.ekfedorov.tm.component.FileScanner;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.exception.incorrect.IncorrectCommandException;
import ru.ekfedorov.tm.repository.*;
import ru.ekfedorov.tm.service.*;
import ru.ekfedorov.tm.util.SystemUtil;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements ServiceLocator, EndpointLocator {

    @NotNull
    public final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    public final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    public final ILoggerService loggerService = new LoggerService();

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    public final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    public final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    public final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    public final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    public final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    public final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    public final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    public final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    public final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    public final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    public final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    public final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Nullable
    private Session session = null;

    private void displayWelcome() {
        loggerService.debug("***           TEST          ***");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
    }

    private void init() {
        initPID();
        initCommands();
        initFileScanner();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.ekfedorov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.ekfedorov.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    private boolean parseArgs(@Nullable final String... args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String param = args[0];
        parseArg(param);
        return true;
    }

    @SneakyThrows
    public void parseCommand(@Nullable final String cmd) {
        if (isEmpty(cmd)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new IncorrectCommandException(cmd);
        command.execute();
    }

    private void process() {
        while (true) {
            try {
                System.out.println();
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        command.setEndpointLocator(this);
        command.setBootstrap(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        displayWelcome();
        init();
        if (parseArgs(args)) new ExitCommand().execute();
        process();
    }

}
