package ru.ekfedorov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.dto.User;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertTrue(userService.findOneById(user1.getId()).isPresent());
        Assert.assertTrue(userService.findOneById(user2.getId()).isPresent());
        userService.remove(users.get(0));
        userService.remove(users.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final User user = new User();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(user.getId()));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void containsTest() {
        final User user = new User();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertTrue(userService.contains(userId));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int userSize = userService.findAll().size();
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertEquals(2 + userSize, userService.findAll().size());
        userService.remove(user1);
        userService.remove(user2);
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() {
        final User user = new User();
        user.setLogin("testFind");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.findByLogin(login).isPresent());
        userService.removeByLogin("testFind");
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final User user = new User();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(userId));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final User user = new User();
        userService.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userService.findOneById(userId).isPresent());
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void isLoginExist() {
        final User user = new User();
        user.setLogin("testExist");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.isLoginExist(login));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        final User user = new User();
        user.setLogin("testRemoveByLogin");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userService.removeByLogin(login);
        Assert.assertFalse(userService.isLoginExist(login));
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final User user = new User();
        userService.add(user);
        final String userId = user.getId();
        userService.removeOneById(userId);
        Assert.assertFalse(userService.findOneById(userId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final User user = new User();
        userService.add(user);
        userService.remove(user);
        Assert.assertNotNull(userService.findOneById(user.getId()));
    }

}
