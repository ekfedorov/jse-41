package ru.ekfedorov.tm.api.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.dto.Project;

import java.util.List;
import java.util.Optional;

public interface IProjectRepository extends IRepository<Project> {

    @Insert("INSERT INTO `app_project` " +
            "(`id`, `name`, `description`, `user_id`, `created`, `date_start`, `date_finish`, `status`) " +
            "VALUES(" +
            "#{id}, #{name}, #{description}, #{userId}, #{created}, #{dateStart}, #{dateFinish}, #{status})")
    void add(@NotNull Project project);

    @SneakyThrows
    @Update("UPDATE `app_project` SET `status` = #{status} " +
            "WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    void changeStatusById(
            @Param("userId") @Nullable String userId,
            @Param("id") @Nullable String id,
            @Param("status") @Nullable Status status);

    @Delete("DELETE FROM `app_project`")
    void clear();

    @Delete("DELETE FROM `app_project` WHERE `user_id` = #{userId}")
    void clearByUserId(@Param("userId") @NotNull String userId);

    @NotNull
    @Select("SELECT * FROM `app_project`")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    List<Project> findAll();

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `user_id` = #{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    List<Project> findAllByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `id` = #{id} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Optional<Project> findOneById(@Param("id") @Nullable String id);

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Optional<Project> findOneByIdAndUserId(
            @Param("userId") @Nullable String userId, @Param("id") @NotNull String id
    );

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `user_id` = #{userId} LIMIT #{index}, 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Optional<Project> findOneByIndex(
            @Param("userId") @Nullable String userId, @Param("index") @NotNull Integer index
    );

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `name` = #{name} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Optional<Project> findOneByName(
            @Param("userId") @Nullable String userId, @Param("name") @NotNull String name
    );

    @Delete("DELETE FROM `app_project` WHERE `id` = #{id}")
    void removeOneById(@Param("id") @Nullable String id);

    @Delete("DELETE FROM `app_project` WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    void removeOneByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM `app_project` WHERE `name` = #{name} AND `user_id` = #{userId} LIMIT 1")
    void removeOneByName(
            @Param("userId") @Nullable String userId, @Param("name") @NotNull String name
    );

    @SneakyThrows
    @Update("UPDATE `app_project` SET `name` = #{name}, `description` = #{description}" +
            " WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    void updateById(
            @Param("userId") @Nullable String userId,
            @Param("id") @Nullable String id,
            @Param("name") @Nullable String name,
            @Param("description") @Nullable String description
    );

}
