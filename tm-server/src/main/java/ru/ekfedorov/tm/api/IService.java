package ru.ekfedorov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface IService<E extends AbstractEntity> {

    void add(@NotNull E entity);

    void addAll(@NotNull List<E> entities);

    void clear();

    boolean contains(@NotNull String id);

    @NotNull
    List<E> findAll();

    @NotNull
    Optional<E> findOneById(@Nullable String id);

    @NotNull
    List<E> findAllSort(@NotNull String sort);

    void removeOneById(@Nullable String id);

}
