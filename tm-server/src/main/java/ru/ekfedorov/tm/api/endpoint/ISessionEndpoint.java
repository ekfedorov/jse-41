package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.UserIsLockedException;
import ru.ekfedorov.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    Session closeSession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws AccessDeniedException;

    @WebMethod
    Session openSession(
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    ) throws UserIsLockedException, AccessDeniedException;

}
