package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.exception.empty.EmailIsEmptyException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.empty.PasswordIsEmptyException;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;
import ru.ekfedorov.tm.dto.Session;
import ru.ekfedorov.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    void createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws AccessDeniedException, LoginIsEmptyException, PasswordIsEmptyException, EmailIsEmptyException;

    @WebMethod
    User findUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    User findUserOneBySession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws NullSessionException, AccessDeniedException;

    @WebMethod
    void setPassword(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable String middleName
    ) throws AccessDeniedException, NullSessionException;

}
