package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.ISessionEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.UserIsLockedException;
import ru.ekfedorov.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws UserIsLockedException, AccessDeniedException {
        return serviceLocator.getSessionService().open(login, password);
    }

    @Override
    @Nullable
    @WebMethod
    public Session closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().close(session);
    }

}
