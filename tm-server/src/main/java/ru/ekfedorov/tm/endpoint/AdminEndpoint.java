package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.IAdminEndpoint;

import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getTaskService().clear();
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getProjectService().clear();
    }

    @Override
    @WebMethod
    public List<Session> listSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    )  throws AccessDeniedException {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getSessionService().findAll();
    }

}
