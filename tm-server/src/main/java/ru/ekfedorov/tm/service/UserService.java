package ru.ekfedorov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.IUserService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.dto.User;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.LoginExistException;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull IPropertyService propertyService,
            @NotNull IConnectionService connectionService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final User user) {
        if (user == null) throw new NullObjectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @SneakyThrows
    @Override
    public void create(
            @Nullable final String login, @Nullable final String password
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
    }

    @SneakyThrows
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (isEmpty(email)) throw new EmailIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
    }

    @SneakyThrows
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
    }

    @SneakyThrows
    @NotNull
    @Override
    public Optional<User> findByLogin(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findByLogin(login);
    }



    @SneakyThrows
    @Override
    public boolean isLoginExist(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findByLogin(login).isPresent();
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Optional<User> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final User user = userOptional.get();
        user.setLock(true);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.lockUnlockUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByLogin(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeByLogin(login);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final Optional<User> user = findOneById(userId);
        if (!user.isPresent()) return;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return;
        user.get().setPasswordHash(hash);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.setPassword(hash, userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Optional<User> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final User user = userOptional.get();
        user.setLock(false);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.lockUnlockUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        @NotNull final Optional<User> user = findOneById(userId);
        if (!user.isPresent()) throw new NullObjectException();
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.updateUser(user.get());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

        @Override
    @SneakyThrows
    public void addAll(@Nullable List<User> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            entities.forEach(userRepository::add);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAllSort(
            @Nullable final String sort
    ) {
        if (isEmpty(sort)) throw new NullComparatorException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<User> comparator = sortType.getComparator();
        return userRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final User entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeOneById(entity.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findAll();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findOneById(id).isPresent();
    }


    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> findOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeOneById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
