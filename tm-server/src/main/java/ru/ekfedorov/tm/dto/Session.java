package ru.ekfedorov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Session extends AbstractEntity implements Cloneable {

    @Column
    @Nullable String signature;

    @Column
    @NotNull Long timestamp = System.currentTimeMillis();

    @Column(name = "user_id")
    @Nullable String userId;

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
