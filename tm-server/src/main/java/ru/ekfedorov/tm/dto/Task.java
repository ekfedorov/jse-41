package ru.ekfedorov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.entity.IWBS;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_task")
public class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public Task(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
