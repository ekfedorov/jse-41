package ru.ekfedorov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_user")
public class User extends AbstractEntity {

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Column
    private boolean lock = false;

    @Column
    @Nullable
    private String login;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Nullable
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

}
