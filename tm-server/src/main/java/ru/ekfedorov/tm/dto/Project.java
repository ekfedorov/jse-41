package ru.ekfedorov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.entity.IWBS;

import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "app_project")
@NoArgsConstructor
public class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
